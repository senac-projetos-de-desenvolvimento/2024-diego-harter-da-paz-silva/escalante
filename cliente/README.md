This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
# or
pnpm dev
# or
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.js`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.


## Dependencias instaladas
├── bootstrap-icons@1.11.3
├── bootstrap@5.3.3
├── eslint-config-next@14.2.3
├── eslint@8.57.0
├── express@4.19.2
├── json-server@1.0.0-beta.1
├── jspdf-autotable@3.8.2
├── jspdf@2.5.1
├── knex@3.1.0
├── next@14.2.3
├── react-dom@18.3.1
├── react-hook-form@7.52.0
├── react-toastify@10.0.5
├── react@18.3.1
├── sweetalert2@11.12.2
└── uuid@10.0.0

## Modificações do dia 11/07/2024
Criado o arquivo Page.jsx na pasta expediente (necessita estilização melhorada);
Criado o arquivo Expediente.module.css na pasta style
Criado o arquivo Pesquisa.jsx na pasta components

## Modificações do dia 15/07/2024 & 16/07/2024
Criado o arquivo Page.jsx na pasta turno
Criado o arquivo Page.jsx na pasta gerenciar-turno -> [turno]
Criado o arquivo Page.jsx na pasta gerenciar -> [id] referente a expediente
Criado o arquivo Cadastro.module.css na pasta style
Criado o arquivo Turnos.module.css na pasta style
Criado o arquivo GerenciarTurnos.module.css na pasta style
Adicionado ao Cadastro opção para cadastrar como Fiscal ou Coordenador

## Modificações do dia 17/07/2024 
Criado o arquivo Page.jsx na pasta operacional, listando todos os GM definindo os coordenadores e fiscais destacando-os em negrito e exportando pdf
Criado o arquivo Operacional.module.css na pasta style
Adicionado ao Titulo.jsx redirecionamento para pagina operacional 
Criado o arquivo Gerenciar.module.css na pasta style
Modificado arquivo page.jsx na pasta expediente
Modificado arquivo page.jsx na pasta gerenciar -> [expediente]
